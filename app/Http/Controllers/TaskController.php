<?php

namespace App\Http\Controllers;

use App\Task;
use App\Projects;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;

class TaskController extends Controller {

    public function create(Request $request) {
        $data = $request->only('nombre', 'fecha_ejecucion','project_id');
        $validator = Validator::make($data, [
                    'nombre' => ['required', 'string', 'max:255', 'unique:Tareas'],                                        
                    'fecha_ejecucion' => ['required', 'date'],
                    'project_id' => ['required', 'integer']
        ]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }

        $task = Projects::where('proyecto_id', $data['project_id'])->first();

        if(is_null($task)){
            return response()->json([
                'success' => false,
                'message' => 'The project does not exist'
                    ], 401); 
        }

        $task->fecha_registro = date("Y-m-d", strtotime($task->fecha_registro));
        $task->fecha_finalizacion = date("Y-m-d", strtotime($task->fecha_finalizacion));
       
        $a=strtotime($data['fecha_ejecucion']);
        $b=strtotime($task->fecha_registro);
        $c=strtotime($task->fecha_finalizacion);       

        if(!($a>=$b && $a<=$c)){
            return response()->json([
                'success' => false,
                'message' => 'The date execution_date must be in the range of project start and end dates.'
                    ], 401);
        }
        
        $token = JWTAuth::getToken();
        if($token){
            $user = JWTAuth::setToken($token)->toUser();
            
            if(!isset($data['descripcion'])){
                $data['descripcion'] = NULL;    
            }

            $validator = Task::create([
                        'nombre' => $data['nombre']
                        , 'descripcion' => $data['descripcion']
                        , 'fecha_ejecucion' => $data['fecha_ejecucion']                        
                        , 'proyecto_id' => $data['project_id']
                        , 'users_id' => $user->id
            ]);
            if ($validator) {
                return response()->json([
                            'success' => true,
                            'message' => 'successfully created task'
                                ], 200);
            }
        }else{
         return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }
    }
    public function update(Request $request,$id) {
        $task = Task::where('tarea_id', $id);
        $getTask= $task->first();
        $data = $request->only('nombre', 'descripcion','fecha_ejecucion');
        $validator = Validator::make($data, [
                    'nombre' => ['required', 'string', 'max:255'],
                    'descripcion' => ['required', 'string', 'max:255'],
                    'fecha_ejecucion' => ['required', 'date']
        ]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        $token = JWTAuth::getToken();        
        $project = Projects::where('proyecto_id', $getTask->proyecto_id)->first();
        $project->fecha_registro = date("Y-m-d", strtotime($project->fecha_registro));
        $project->fecha_finalizacion = date("Y-m-d", strtotime($project->fecha_finalizacion));
       
        $a=strtotime($data['fecha_ejecucion']);
        $b=strtotime($project->fecha_registro);
        $c=strtotime($project->fecha_finalizacion);       

        if(!($a>=$b && $a<=$c)){
            return response()->json([
                'success' => false,
                'message' => 'The date execution_date must be in the range of project start and end dates.'
                    ], 401);
        }

        if($token){
            $task->update([
                'nombre' => $data['nombre'],
                'descripcion' => $data['descripcion'],
                'fecha_ejecucion' => $data['fecha_ejecucion']
                ]);
            if ($validator) {
                return response()->json([
                            'success' => true,
                            'message' => 'successfully modified task'
                                ], 200);
            }
        }else{
            return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }
    }
    public function delete(Request $request,$id) {
        $task = Task::where('tarea_id', $id);    
        if($task){
            $token = JWTAuth::getToken();        
            if($token){            
                if ($task->delete()) {
                    return response()->json([
                                'success' => true,
                                'message' => 'task successfully deleted'
                                    ], 200);
                }
            }else{
                return response()->json([
                'success' => false,
                'message' => 'Token not valid'
                    ], 401);
            }
        }
        return response()->json([
            'success' => false,
            'message' => 'task does not exist'
                ], 401);        
    }
    public function completeProject(Request $request) {
        $data = $request->only('tarea_id');
        $validator = Validator::make($data, ['tarea_id' => ['required', 'integer']]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        $token = JWTAuth::getToken();        

        if($token){
            $task = Task::where('tarea_id', $data['tarea_id']);
            $getTask = $task->first();
                if($getTask->estado ==='0'){
                    return response()->json([
                    'success' => false,
                    'message' => 'The task is finished'
                        ], 401);
                }

            $task->update(['estado' => 0]);
            if ($validator) {
                return response()->json([
                            'success' => true,
                            'message' => 'Finished task'
                                ], 200);
            }
        }else{
            return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }       
    }
    

}
