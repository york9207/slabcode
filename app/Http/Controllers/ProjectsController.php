<?php

namespace App\Http\Controllers;

use App\Projects;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;

class ProjectsController extends Controller {

    public function create(Request $request) {
        $data = $request->only('nombre', 'descripcion', 'fecha_inicio','fecha_finalizacion');
        $validator = Validator::make($data, [
                    'nombre' => ['required', 'string', 'max:255', 'unique:Proyectos'],
                    'fecha_inicio' => ['required', 'date'],
                    'fecha_finalizacion' => ['required', 'date']                    
        ]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        
        $a=strtotime($data['fecha_inicio']);
        $b=strtotime($data['fecha_finalizacion']);

        if($b <= $a){
            return response()->json([
                'success' => false,
                'message' => 'The end date must be greater than the initial date of the project'
                    ], 200);
        }

        $token = JWTAuth::getToken();
        if($token){
            $user = JWTAuth::setToken($token)->toUser();

            $validator = Projects::create([
                        'nombre' => $data['nombre']
                        , 'descripcion' => $data['descripcion']
                        , 'fecha_inicio' => $data['fecha_inicio']
                        , 'fecha_finalizacion' => $data['fecha_finalizacion']
                        , 'users_id' => $user->id
            ]);
            if ($validator) {
                return response()->json([
                            'success' => true,
                            'message' => 'successfully created project'
                                ], 200);
            }
        }else{
         return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }
    }
    public function update(Request $request,$id) {
        $proyecto = Projects::where('proyecto_id', $id);

        $data = $request->only('nombre', 'descripcion','fecha_finalizacion');
        $validator = Validator::make($data, [
                    'nombre' => ['required', 'string', 'max:255'],
                    'fecha_finalizacion' => ['required', 'date']
        ]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        $token = JWTAuth::getToken();        
        if($token){
            $vectorAModificar= array(                
                    'nombre' => $data['nombre'],
                    'descripcion' => $data['descripcion']                    
            );

            $count = Task::where('fecha_ejecucion' ,'>', $data['fecha_finalizacion'])->count();
            
            if($count === 0){
                $vectorAModificar['fecha_finalizacion'] = $data['fecha_finalizacion'];
            }
                                    
            if ($proyecto->update($vectorAModificar)) {
                return response()->json([
                            'success' => true,
                            'message' => 'successfully modified project'
                                ], 200);
            }            
        }else{
            return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }
    }
    public function delete(Request $request,$id) {
        $proyecto = Projects::where('proyecto_id', $id);        
        if($proyecto){
            $token = JWTAuth::getToken();        
            if($token){            
                if ($proyecto->delete()) {
                    return response()->json([
                                'success' => true,
                                'message' => 'project successfully deleted'
                                    ], 200);
                }
            }else{
                return response()->json([
                'success' => false,
                'message' => 'Token not valid'
                    ], 401);
            }
        }
        return response()->json([
            'success' => false,
            'message' => 'project does not exist'
                ], 401);        
    }
    public function completeProject(Request $request) {
        $data = $request->only('proyecto_id');
        $validator = Validator::make($data, ['proyecto_id' => ['required', 'integer']]);
       
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        $token = JWTAuth::getToken();        
        if($token){
            $proyecto = Projects::where('proyecto_id', $data['proyecto_id'])->where('estado',1);
            $getProyecto = $proyecto->count();

            if($getProyecto===0){
                return response()->json([
                    'success' => false,
                    'message' => "the project is finalized"
                        ], 401);   
            }

            $count = Task::where('proyecto_id' , $data['proyecto_id'])->where('estado', 1)->count();
            if($count === 0){                
                if ($proyecto->update(['estado'=>0])) {
                    return response()->json([
                                'success' => true,
                                'mail' => 'Mail sending is simulated',
                                'message' => 'completed project'
                                    ], 200);
                }            
            }else{
                return response()->json([
                    'success' => false,
                    'message' => "the project can't be finalized"
                        ], 401);    
            }
        }else{
            return response()->json([
            'success' => false,
            'message' => 'Token not valid'
                ], 401);
        }            
    }
    

}
