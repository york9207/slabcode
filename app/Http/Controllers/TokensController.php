<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;

class TokensController extends Controller {

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
                    'email' => 'required|email',
                    'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 1,
                        'message' => 'Wrong validation',
                        'errors' => $validator->errors()
                            ], 422);
        }
        $user = User::where('email',$credentials['email'])->first();
        
        if($user->administrador ==='0'){
            if($user->estado === '0'){
                return response()->json([
                    'success' => false,
                    'code' => 2,
                    'message' => 'access denied'], 401);
                }
        }               
        $token = JWTAuth::attempt($credentials);

        if ($token) {
            return response()->json([
                        'idToken' => $token,
                        'id' => $user->id,
                        'nombre' => $user->nombre,
                        'email' => $user->email,
                        'administrador' => $user->administrador,
                        'expiresIn' => "36000"], 200);
        } else {
            return response()->json([
                        'success' => false,
                        'code' => 2,
                        'message' => 'Wrong credentials',
                        'errors' => $validator->errors()], 401);
        }
    }

    public function refreshToken() {

        $token = JWTAuth::getToken();

        try {
            $token = JWTAuth::refresh($token);
            return response()->json(['success' => true, 'idToken' => $token], 200);
        } catch (TokenExpiredException $ex) {
            // We were unable to refresh the token, our user needs to login again
            return response()->json([
                        'code' => 3, 'success' => false, 'message' => 'Need to login again, please (expired)!'
            ]);
        } catch (TokenBlacklistedException $ex) {
            // Blacklisted token
            return response()->json([
                        'code' => 4, 'success' => false, 'message' => 'Need to login again, please (blacklisted)!'
                            ], 422);
        }
    }

    public function logout() {
        $token = JWTAuth::getToken();
        try {
            $token = JWTAuth::invalidate($token);
            return response()->json([
                        'code' => 5, 'success' => true, 'message' => "You have successfully logged out."
                            ], 200);
        } catch (JWTException $e) {
            return response()->json([
                        'code' => 6, 'success' => false, 'message' => 'Failed to logout, please try again.'
                            ], 422);
        }
    }

    public function create(Request $request) {
        $data = $request->only('nombre', 'administrador', 'email', 'password');
        $validator = Validator::make($data, [
                    'nombre' => ['required', 'string', 'max:255'],
                    'administrador' => ['required', 'integer'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'message' => $validator->errors()
                            ], 422);
        }
        $validator = User::create([
                    'nombre' => $data['nombre'],
                    'administrador' => $data['administrador'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
        ]);
        if ($validator) {
            return response()->json([
                        'success' => true,
                        'message' => 'successfully created user',
                        'mailer' => 'The simulation of sending mail is carried out by the user creation'
                            ], 200);
        }
    }

    public function forgetPassword(Request $request) {
        $data = $request->only('email', 'current_password','new_password');

        $validator = Validator::make($data, [
                    'email' => 'required|email',
                    'current_password' => 'required',
                    'new_password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 1,
                        'message' => 'Wrong validation',
                        'errors' => $validator->errors()
                            ], 422);
        }

        if ($data['current_password'] === $data['new_password']) {
            return response()->json([
                'success' => false,
                'code' => 2,
                'message' => 'Passwords should not be the same',
                'errors' => $validator->errors()], 401);
        }else{
            $user = User::where('email', $data['email'])->first();

            if (password_verify($data['current_password'],$user->password)) {
                $user = User::where('email', $data['email'])->update(['password'=> Hash::make($data['new_password'])]);          
                return response()->json([
                    'success' => true,
                    'message' => 'password successfully modified'
                        ], 200);
            } else {
                return response()->json([
                            'success' => false,
                            'code' => 2,
                            'message' => 'Wrong credentials',
                            'errors' => $validator->errors()], 401);
            }
        }
    }
    public function enabledOperario(Request $request) {
        $data = $request->only('email','estado');

        $validator = Validator::make($data, ['email' => ['required', 'email'],'estado' => ['required', 'integer']]);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'code' => 1,
                        'message' => 'Wrong validation',
                        'errors' => $validator->errors()
                            ], 422);
        }       
        $token = JWTAuth::getToken();        

        if($token){
            $user = User::where('email',$data['email'])->first();
            if($user->administrador === '0'){               
                $validator = $user->update([
                    'estado' => $data['estado']
                ]);
                if ($validator) {
                    return response()->json([
                        'success' => true,
                        'message' => 'Modified operator'], 200);
                }                          
            }else{
                return response()->json(['success' => false,'message' => 'It is not allowed to change status'], 401);    
            }
        }else{
            return response()->json(['success' => false,'message' => 'Token not valid'], 401);
        }   
    }
}
