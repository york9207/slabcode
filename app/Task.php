<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $table = 'tareas';
    public $timestamps = false;
    protected $fillable = [
        'nombre'
        , 'descripcion'
        , 'fecha_registro'
        , 'fecha_ejecucion'        
        , 'proyecto_id'
        , 'users_id'
    ];

}
