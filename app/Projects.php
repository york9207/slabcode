<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model {

    protected $table = 'proyectos';
    public $timestamps = true;
    protected $fillable = [
         'nombre'
        , 'descripcion'
        , 'fecha_registro'
        , 'fecha_finalizacion'        
        , 'users_id'
    ];

}
