<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'v1'], function () {
    //Projects
    Route::post('/projects/create', 'ProjectsController@create');
    Route::put('/projects/update/{id}', 'ProjectsController@update');
    Route::delete('/projects/delete/{id}', 'ProjectsController@delete');
    Route::post('/projects/completeProject', 'ProjectsController@completeProject');

    //Task
    Route::post('/task/create', 'TaskController@create');
    Route::put('/task/update/{id}', 'TaskController@update');
    Route::delete('/task/delete/{id}', 'TaskController@delete');
    Route::post('/task/completeProject', 'TaskController@completeProject');


    //Admin
    Route::post('/admin/enabledOperario', 'TokensController@enabledOperario');

});

Route::group(['middleware' => [], 'prefix' => 'v1'], function () {

    //Create User
    Route::post('/users/createUser', 'TokensController@create');

    // Auth
    Route::post('/auth/login', 'TokensController@login');
    Route::post('/auth/refresh', 'TokensController@refreshToken');
    Route::post('/auth/logout', 'TokensController@logout');

    //Forget Password
    Route::post('/users/forgetPassord', 'TokensController@forgetPassword');
});



